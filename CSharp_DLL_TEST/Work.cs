﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CSharp_DLL_TEST
{
    class Work
    {
        private delegate string delegateadd(string md5check);

        private delegateadd upgradeDll;

        private int hModule;

        public void workfunction()
        {
            if (this.LoadDll("DLL_DECRYPT.dll"))
            {
                try
                {
                    string asd = this.upgradeDll("3LYuFj/CVRfCpp6Hqh79JUQ=");
                }
                catch(SystemException e)
                {
                    Console.Write(e.ToString());
                }
                DLLWrapper.FreeLibrary(this.hModule);
            }
            else
            {
                Console.WriteLine("DLL加载失败");
                Console.ReadKey();
            }
        }

        private bool LoadDll(string dllpath)
        {
            int hModule = DLLWrapper.LoadLibrary(dllpath);

            if (hModule == 0)
                return false;
            else
            {
                this.upgradeDll = (delegateadd)DLLWrapper.GetFunctionAddress(hModule, "decryptString", typeof(delegateadd));
                if (this.upgradeDll == null)
                {
                    DLLWrapper.FreeLibrary(hModule);
                    return false;
                }
            }
            this.hModule = hModule;
            return true;
        }
    }
}
